#pragma once
#include <glm/glm.hpp>

class Camera {
public:
	Camera(glm::vec3 pos, glm::vec3 target, glm::vec3 up);
	void move(glm::vec3 dvec);
	void roll(float angle);
	glm::mat4 getViewMatrix() { return viewMatrix; }
private:
	void setViewMatrix();
	glm::vec3 p;
	glm::vec3 u;
	glm::vec3 v;
	glm::vec3 n;
	glm::mat4 viewMatrix = glm::mat4(0.0f);
};