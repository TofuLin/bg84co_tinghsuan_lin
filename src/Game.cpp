#include "Game.h"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

// iostream to access cout
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <fstream>


#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <Extras/Serialize/BulletWorldImporter/btBulletWorldImporter.h>

using namespace std;



// ************* Game ***************

void Game::initialise() {
	// Initialise SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	screenWidth = 1000;
	screenHeight = 750;


	// Set up an OpenGL window and context
	std::cout << "Creating the game world now...\n" << std::endl;
	window = SDL_CreateWindow("Dice or Die", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	context = SDL_GL_CreateContext(window);

	// Initialise GLEW
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK) {
		std::cout << "GLEW Error" << glewGetString(GLEW_VERSION) << std::endl;
	}

	// Default config file
	// Should allow this to be set from the command line
	configFile = "assets/config.txt";

	// Read configuration file
	readConfigFile();

	// Load assets
	loadAssets();

	// Bind keyboard/mouse/gamepad
	bindKeyboard();

	// Massage game objects if required

	// Set up OpenGL buffers for mesh objects
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Send game objects to GPU
	for (unsigned int i = 0; i < mVisibleWorld.size(); ++i) {
		mVisibleWorld[i]->sendObjectToGPU();
	}

	// Set up a light for the world
	// Arbitrary light position and colour values
	theLight.position = glm::vec4(-5.0f, 5.0f, -4.0f, 1.0f);
	theLight.ambientColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	theLight.diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	theLight.specularColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	// Default view and projection matrices
	view = glm::lookAt(glm::vec3(0.0, 10.0, 20.0), glm::vec3(0.0, 0.0, -4.0), glm::vec3(0.0, 1.0, 0.0));
	projection = glm::perspective(45.0f, 1.0f*screenWidth / screenHeight, 1.0f, 100.0f);

	// Top down camera
	topDownCamera = new Camera(glm::vec3(0.0, -10.0, -20.0),
		glm::vec3(0.0, 0.0, -4.0),
		glm::vec3(0.0, 1.0, 0.0));

	// Default shaders

	try {
		defaultShader = new ShaderProgram();
		defaultShader->initFromFiles("assets/default.vert", "assets/default.frag");
		defaultShader->addAttribute("vPosition");
		defaultShader->addAttribute("vNormal");
		defaultShader->addUniform("P");
		defaultShader->addUniform("M");
		defaultShader->addUniform("V");
		defaultShader->addUniform("lightPosition");
		defaultShader->addUniform("ambientContrib");
		defaultShader->addUniform("diffuseContrib");
		defaultShader->addUniform("specularContrib");
		defaultShader->addUniform("shininess");

		defaultShader->use();
	}
	catch (const runtime_error& error) {
		cerr << "Error in shader processing!" << endl;
		cerr << error.what();
		// Program now continues but will abort when it hits the code to associate 
		// attribute/uniforms with appropriate program data as that code
		// has not been wrapped in a try..catch block.
		// Might be better to terminate program at this point.
	}

	glEnable(GL_DEPTH_TEST);

	// Initialise physics world
	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
	btCollisionDispatcher* dispatcher = new	btCollisionDispatcher(collisionConfiguration);
	btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;
	thePhysicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache,
		solver, collisionConfiguration);

	isPhysicsPaused = true;

	// Associate visible world and physics world

	btBulletWorldImporter* fileLoader = new btBulletWorldImporter(thePhysicsWorld);

	//optionally enable the verbose mode to provide debugging information during file loading (a lot of data is generated, so this option is very slow)
	//fileLoader->setVerboseMode(true);

	fileLoader->loadFile(physicsFilename.c_str());

	int numRigidBodies = fileLoader->getNumRigidBodies();
	for (int i = 0; i < numRigidBodies; i++) {
		// Identify rigid bodies in physics world
		btCollisionObject* coll = fileLoader->getRigidBodyByIndex(i);
		btRigidBody* body = btRigidBody::upcast(coll);
		string bodyName = fileLoader->getNameForPointer(body);
		std::cout << "Body name: " << bodyName << endl;
		rigidBodyNames[body] = bodyName;
		if (bodyName == "Tordo") {
			theTordo = body;
			theTordoStartPos = theTordo->getCenterOfMassPosition();
			theTordoStartOrientation = theTordo->getOrientation();
			theTordo->setRestitution(0.85);
			int cflag = theTordo->getCollisionFlags();
			theTordo->setActivationState(DISABLE_DEACTIVATION);
			theTordo->setDamping(1.0, 1.0);
			theTordo->setAngularFactor(0.0);
			// Camera values
			//cameraPos = glm::vec3(theTordoStartPos.getX()*0.5, theTordoStartPos.getY()*(-10), theTordoStartPos.getZ()*(-0.5));
			cameraPos = glm::vec3(theTordoStartPos.getX(), theTordoStartPos.getY(), theTordoStartPos.getZ());
			cameraLookDirection = cameraPos;
			cameraOffset = glm::vec3(0.0f, 1.5f, 0.75f);
			cameraPos = cameraPos + cameraOffset;
			cameraUp = glm::vec3(0.0f, 0.0f, 1.0f);
		}
		if (bodyName == "Dice1") {
			theDice1 = body;
			btTransform theDice1StartTransform = theDice1->getWorldTransform();

			theDice1StartPos = theDice1->getCenterOfMassPosition();
			theDice1StartOrientation = theDice1->getOrientation();

			btDefaultMotionState *ms = new btDefaultMotionState(theDice1StartTransform);
			theDice1->setMotionState(ms);
		}
		body->setRestitution(0.85);
//		collisionCallback.bodyNames = &rigidBodyNames;
		if (bodyName == "Dice2") {
			theDice2 = body;
			theDice2StartPos = theDice2->getCenterOfMassPosition();
			theDice2StartOrientation = theDice2->getOrientation();
			theDice2->setRestitution(0.55);
		}
		body->setRestitution(0.55);
//		collisionCallback.bodyNames = &rigidBodyNames;
	}
	thePhysicsWorld->setGravity(btVector3(0.0, 0.0, -9.8));

	// Physics world and OBJ file as exported from Blender
	// has Z-up and Y-forward, need to translate to
	// OpenGL world with -Z-forward and Y-up.
	// This transform will do the trick.

	phys2VisTransform = glm::mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

}

void Game::run()
{
	PlayerData player;
	player = { "Tordo",5000,100};
	if (music = true) {
		PlaySound("assets/Recording_cut.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	}

	int i, roll;
	std::string rollnum;
	SDL_Event windowEvent;
	float lastTime = SDL_GetTicks() / 1000.0f;
	while (true)
	{
		float current = SDL_GetTicks() / 1000.0f;
		float delta = current - lastTime;
		SDL_Keycode theKey = SDLK_UNKNOWN;

		if (SDL_PollEvent(&windowEvent))
		{
			if (windowEvent.type == SDL_QUIT) break;
			if (windowEvent.type == SDL_KEYUP &&
				windowEvent.key.keysym.sym == SDLK_ESCAPE) break;

			if (windowEvent.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
				std::cout << "Window resized." << std::endl;
				screenWidth = windowEvent.window.data1;
				screenHeight = windowEvent.window.data2;
				glViewport(0, 0, screenWidth, screenHeight);
			}

			if (windowEvent.type == SDL_KEYDOWN) {
				theKey = windowEvent.key.keysym.sym;
				switch (theKey) {
				case SDLK_SPACE:

					roll = (rand() % 6 + 1) + (rand() % 6 + 1);
					std::cout << roll << endl;
					rollnum = std::to_string(roll);
					//MessageBox(NULL, rollnum.c_str(), "roll the dice", MB_OK | MB_ICONASTERISK);
                     if (MessageBox(NULL, rollnum.c_str(), "roll the dice", MB_OK | MB_ICONASTERISK)==IDOK) {
						movestep += roll;
						if(movestep<=36) {
							movestep = movestep;
							std::cout << "Steps:" << movestep << endl;
							if (movestep == 1 || movestep == 19) {
								MessageBox(NULL, "You can accept a quest,would you?", "Quest", MB_YESNO | MB_ICONQUESTION);
								std::cout << "Quest!" << endl;
							}
							else if (movestep == 5 || movestep == 32) {
								MessageBox(NULL, "Let's play a game", "Game time", MB_OK | MB_ICONHAND);
								std::cout << "Game time,let's play a game" << endl;
							}
							else if (movestep == 9 || movestep == 22 || movestep == 27) {
								MessageBox(NULL, "Would you buy something?", "Shop", MB_YESNO | MB_ICONQUESTION);
								std::cout << "Shop." << endl;
							}
							else if (movestep == 18) {
								MessageBox(NULL, "Would you fly to another place?", "Airport", MB_YESNO | MB_ICONQUESTION);
								std::cout << "You are now at airport,you can choose one classroom and fly to there." << endl;
							}
							else if (movestep == 4 || movestep == 8 || movestep == 12 || movestep == 13 || movestep == 17 || movestep == 23 || movestep == 26 || movestep == 28 || movestep == 33) {
								MessageBox(NULL, "You are embroiled in the war!", "Oops", MB_OK | MB_ICONQUESTION);
								std::cout << "You are embroiled in the war." << endl;
							}
							else {
								//MessageBox(NULL, "You can buy a house,would you?", "House trade", MB_YESNO | MB_ICONASTERISK);
								std::cout << "You can buy a house." << endl;
								if (MessageBox(NULL, "You can buy a house", "House trade", MB_YESNO | MB_ICONASTERISK) == IDYES) {
									player.money = player.money - 500;
									cout << "You pay 500,so now you have:" << player.money << endl;
									if (player.money <= 0) {
										cout << "You are broke." << endl;
										MessageBox(NULL, "You are broke", "Broke", MB_OK | MB_ICONASTERISK);
									}
								}
							}

					}
						else if (movestep >= 36) {
							movestep = movestep - 36;
							std::cout << "Steps:" << movestep << endl;
							if (movestep == 0) {
								//MessageBox(NULL, "You earn $100", "Back to the start point", MB_OK | MB_ICONASTERISK);
								std::cout << "Welcome back!" << endl;
								if (MessageBox(NULL, "You earn $100", "Back to the start point", MB_OK | MB_ICONASTERISK) == IDOK) {
									player.money = player.money + 100;
									cout << player.money << endl;
								}

							}
							else if (movestep == 1 || movestep == 19) {
								MessageBox(NULL, "You can accept a quest,would you?", "Quest", MB_YESNO | MB_ICONQUESTION);
								std::cout << "Quest!" << endl;
							}
							else if (movestep == 5 || movestep == 32) {
								MessageBox(NULL, "Let's play a game", "Game time", MB_OK | MB_ICONHAND);
								std::cout << "Game time,let's play a game" << endl;
							}
							else if (movestep == 9 || movestep == 22 || movestep == 27) {
								MessageBox(NULL, "Would you buy something?", "Shop", MB_YESNO | MB_ICONQUESTION);
								std::cout << "Shop." << endl;
							}
							else if (movestep == 18) {
								MessageBox(NULL, "Would you fly to another place?", "Airport", MB_YESNO | MB_ICONQUESTION);
								std::cout << "You are now at airport,you can choose one classroom and fly to there." << endl;
							}
							else if (movestep == 4 || movestep == 8 || movestep == 12 || movestep == 13 || movestep == 17 || movestep == 23 || movestep == 26 || movestep == 28 || movestep == 33) {
								MessageBox(NULL, "You are embroiled in the war!", "Oops", MB_OK | MB_ICONQUESTION);
								std::cout << "You are embroiled in the war." << endl;
							}
							else {
								//MessageBox(NULL, "You can buy a house,would you?", "House trade", MB_YESNO | MB_ICONASTERISK);
								std::cout << "You can buy a house." << endl;
								if (MessageBox(NULL, "You can buy a house", "House trade", MB_YESNO | MB_ICONASTERISK) == IDYES) {
									player.money = player.money - 500;
									cout << "You pay 500,so now you have:" << player.money << endl;
								}
							}
						}
					}
						
					break;

				case SDLK_a:
					if (useTopDownCamera)
						topDownCamera->move(glm::vec3(1, 0, 0));
					else TordoMove();
						std::cout << "A" << endl;
					break;
					
				case SDLK_d:
					if (useTopDownCamera)
						topDownCamera->move(glm::vec3(-1, 0, 0));
					else
						std::cout << "D" << endl;

					break;
				case SDLK_w:
					if (useTopDownCamera)
						topDownCamera->move(glm::vec3(0, 1, 0));
					else
						std::cout << "W" << endl;
					TordoMove();
					break;
				case SDLK_s:
					if (useTopDownCamera)
						topDownCamera->move(glm::vec3(0, -1, 0));
					else
						std::cout << "S" << endl;

					break;
				case SDLK_z:
					if (useTopDownCamera)
						topDownCamera->move(glm::vec3(0, 0, 1));
					else
						view = glm::translate(view, glm::vec3(0.0f, 0.0f, 0.5f));
					std::cout << "Zoom in" << endl;

					break;
				case SDLK_x:
					if (useTopDownCamera)
						topDownCamera->move(glm::vec3(0, 0, -1));
					else
						view = glm::translate(view, glm::vec3(0.0f, 0.0f, -0.5f));
					std::cout << "Zoom out" << endl;

					break;

				case SDLK_g:
					correctForBullet = !correctForBullet;
					break;
				
				case SDLK_m:
					PlaySound(NULL, NULL, SND_FILENAME);
					std::cout << "Stop the music" << endl;
					break;

				case SDLK_n:
					PlaySound("assets/Recording_cut.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
					std::cout << "Play the music" << endl;
					break;

				//case SDLK_h:
				//	window = SDL_CreateWindow("Dice or Die", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 768, 768, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
				//	break;

				//case SDLK_y:
				//	window = SDL_CreateWindow("Dice or Die", 500, 100, screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
				//	break;

				}

			}

		}
		// Update game in response to user input
		update(theKey, delta);

		// Render the game world.
		render();


		lastTime = current;
	}
}

void Game::shutdown()
{
	// Shutdown
	SDL_GL_DeleteContext(context);
	SDL_Quit();
}

void Game::readConfigFile()
{
	string line;
	string key;
	ifstream ifs(configFile, ifstream::in);

	while (ifs.good() && !ifs.eof() && getline(ifs, line))
	{
		string configType = "";
		stringstream str(line);
		str >> configType;
		if (configType == "#") // a comment line
			continue;
		if (configType == "Asset") { // An asset line gives the name (with path) of file containing
									 // assets (ie 3D object meshes)
			string fname;
			str >> fname;
			size_t lastSlash = fname.find_last_of("/\\");
			assetFiles.push_back(fname);
		}
		if (configType == "Physics") {  // where to find bullet physics file
			str >> physicsFilename;
		}
		else if (configType == "Key") {
			string key;
			string binding;
			str >> key >> binding;
			keyBindings[key] = binding;
		}
	}
}
void Game::bindKeyboard()
{
	/*KeyHandler *forward = new KeyHandler([this](float d) {
		if (useTopDownCamera)
			topDownCamera->move(glm::vec3(0, 0, -1));
		else TordoMove();
	});
	commandHandler["forward"] = forward;*/
	KeyHandler *togglePhysics = new KeyHandler([this](float d) {
		isPhysicsPaused = !isPhysicsPaused;
	});
	commandHandler["togglePhysics"] = togglePhysics;

	KeyHandler *toggleTopDownCamera = new KeyHandler([this](float d) {
		useTopDownCamera = !useTopDownCamera;
		std::cout << "Change camera" << endl;
	});
	commandHandler["useTopDownCamera"] = toggleTopDownCamera;
	KeyHandler *correctBullet = new KeyHandler([this](float d) {
		correctForBullet = !correctForBullet;
	});
	commandHandler["correctBullet"] = correctBullet;

}


void Game::loadAssets()
{
	// process each asset file listed in assetFiles
	Assimp::Importer importer;
	for (int i = 0; i < assetFiles.size(); ++i) {

		// Each asset file is read by an Assimp importer
		// No post processing requested -- assuming objects exported from 3D modeller as required for game
		const aiScene* scene = importer.ReadFile(assetFiles[i], 0);
		if (!scene) {
			fprintf(stderr, importer.GetErrorString());
		}

		// Each asset file can contain the meshes and material/texture descriptions for multiple objects.
		// I am assuming that asset files are Wavefront OBJ files (but Assimp can handle a great many
		// other formats)
		// The way to handle this is to process the aiNode tree -- but as OBJ files cannot describe
		// hierarchical relationships between objects this tree is just one layer deep.
		// We can thus process the scene as follows (this would not work for a scene containing
		// a hierarchy of meshes).

		aiNode* rn = scene->mRootNode;

#ifdef _DEBUG
		std::cout << "Mesh source: " << assetFiles[i] << endl;
		std::cout << "  Root node: " << rn->mName.data << endl;
		std::cout << "  Num children: " << rn->mNumChildren << endl;
		for (unsigned int i = 0; i < rn->mNumChildren; ++i) {
			const aiNode* cn = rn->mChildren[i];
			std::cout << "    C" << i << " Name: " << cn->mName.data << endl;
			std::cout << "       Num children: " << cn->mNumChildren << endl;
			std::cout << "       Num meshes: " << cn->mNumMeshes << endl;
			std::cout << "       Mesh index: " << cn->mMeshes[0] << endl;
			int matIndex = scene->mMeshes[cn->mMeshes[0]]->mMaterialIndex;
			std::cout << "       Mesh material index: " << matIndex << endl;
			aiString matname;
			scene->mMaterials[matIndex]->Get(AI_MATKEY_NAME, matname);
			std::cout << "       Material name: " << matname.data << endl;
			aiColor3D colorD(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, colorD);
			std::cout << "       Material diffusive colour: " << colorD.r << "," << colorD.g << "," << colorD.b << endl;
			aiColor3D colorA(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_AMBIENT, colorA);
			std::cout << "       Material ambient colour: " << colorA.r << "," << colorA.g << "," << colorA.b << endl;
			aiColor3D colorS(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_SPECULAR, colorS);
			std::cout << "       Material specular colour: " << colorS.r << "," << colorS.g << "," << colorS.b << endl;
			float shininess = 0.0;
			scene->mMaterials[matIndex]->Get(AI_MATKEY_SHININESS, shininess);
			// For some reason the Get function returns a value for shininess that is 4 times too large!
			std::cout << "       Material specular shininess: " << (shininess / 4.0) << endl;
		}
#endif

		for (unsigned int i = 0; i < rn->mNumChildren; ++i) {
			const aiNode* cn = rn->mChildren[i];
			// Construct a GameObject from the child.
			// I will give the GameObject the name of the child,
			// extract the mesh and material definitions (if they exist)
			string name = cn->mName.data;
			// Assume there is only one mesh associated with object (note that some models
			// can have more than one mesh!)
			unsigned int meshIndex = cn->mMeshes[0];
			aiMesh* theSceneMesh = scene->mMeshes[meshIndex];

			Mesh * objMesh = new Mesh();

			// Get vertices for mesh
			objMesh->vertices.reserve(theSceneMesh->mNumVertices);
			for (unsigned int i = 0; i < theSceneMesh->mNumVertices; i++) {

				aiVector3D pos = theSceneMesh->mVertices[i];
				objMesh->vertices.push_back(glm::vec4(pos.x, pos.y, pos.z, 1.0f));
			}

			// Get normals for mesh
			if (theSceneMesh->HasNormals()) {
				objMesh->normals.reserve(theSceneMesh->mNumVertices);
				for (unsigned int i = 0; i < theSceneMesh->mNumVertices; i++) {
					aiVector3D n = theSceneMesh->mNormals[i];
					objMesh->normals.push_back(glm::vec3(n.x, n.y, n.z));
				}
			}

			// Fill face indices
			if (theSceneMesh->HasFaces()) {
				objMesh->elements.reserve(3 * theSceneMesh->mNumFaces);
				for (unsigned int i = 0; i < theSceneMesh->mNumFaces; i++) {
					// Assume the model has only triangles.
					objMesh->elements.push_back(theSceneMesh->mFaces[i].mIndices[0]);
					objMesh->elements.push_back(theSceneMesh->mFaces[i].mIndices[1]);
					objMesh->elements.push_back(theSceneMesh->mFaces[i].mIndices[2]);
				}
			}

			// Fill vertices texture coordinates
			if (theSceneMesh->mTextureCoords[0] != NULL) {
				objMesh->texcoords.reserve(theSceneMesh->mNumVertices);
				for (unsigned int i = 0; i < theSceneMesh->mNumVertices; i++) {
					aiVector3D  UVW = theSceneMesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
					objMesh->texcoords.push_back(glm::vec2(UVW.x, UVW.y));
				}
			}

			Material* objMaterial = new Material();

			// Now get material associated with mesh
			int matIndex = scene->mMeshes[meshIndex]->mMaterialIndex;

			aiColor3D colorA(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_AMBIENT, colorA);
			objMaterial->ambientReflectivity = glm::vec4(colorA.r, colorA.g, colorA.b, 1.0f);

			aiColor3D colorD(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, colorD);
			objMaterial->diffuseReflectivity = glm::vec4(colorD.r, colorD.g, colorD.b, 1.0f);

			aiColor3D colorS(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_SPECULAR, colorS);
			objMaterial->specularRelectivity = glm::vec4(colorS.r, colorS.g, colorS.b, 1.0f);
			float shininess = 0.0;
			scene->mMaterials[matIndex]->Get(AI_MATKEY_SHININESS, shininess);
			// For some reason the Get function returns a value for shininess that is 4 times too large!
			objMaterial->shininess = shininess / 4.0f;

			// Finally add object to world
			mVisibleWorld.push_back(new VisibleObject(name, objMesh, objMaterial));


		}
	}

}

void Game::update(SDL_Keycode aKey, float delta)
{
	if (aKey != SDLK_UNKNOWN) {
		string kname = SDL_GetKeyName(aKey);
		// check to see if kname is actually bound to a command
		// If it is we assume that an action has been associated with the command and run it
		if (keyBindings.find(kname) != keyBindings.end()) {
			string command = keyBindings[kname];
			(*commandHandler[command])(delta);
			// commandHandler[command]->run(delta);

		}
	}

	// Update physics world
	if (!isPhysicsPaused) {
		thePhysicsWorld->stepSimulation(delta, 10);
	}
}

void Game::render()
{
	//SDL_Color color = { 255, 0, 0, 0 }; // Red
										//RenderText("Hello", color, 5, 10, 12);
										// Display model
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	/*RenderString(0.0f, 0.0f, "");*/
	//PlayerData player;
	//player = { "Tordo",3000,0 };
	/*pFont20Pt = loadFont("assets/fonts/coburn.ttf", 20);
	pPlayerNameTex = createTextTexture("Player: " + player.name, pFont20Pt, GOLD, TTF_STYLE_NORMAL);*/
	for (int i = 0; i < mVisibleWorld.size(); ++i) {
		if (mVisibleWorld[i]->isVisible()) {
			if (mVisibleWorld[i]->getName() == "Tordo") {
				btTransform TordoTransform;
				TordoTransform = theTordo->getWorldTransform();
				btVector3 TordoPos = TordoTransform.getOrigin();
				btQuaternion TordoQuat = TordoTransform.getRotation();
				btQuaternion tq = TordoQuat * theTordoStartOrientation.inverse();
				btVector3 rotAxis = tq.getAxis();
				btScalar rot = tq.getAngle();

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(TordoPos.getX(), TordoPos.getY(), TordoPos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f)*theTordoStartPos.getX(), (-1.0f)*theTordoStartPos.getY(), (-1.0f)*theTordoStartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);

				// Update camera
				cameraPos = glm::vec3(theTordoStartPos.getX(), theTordoStartPos.getY(), theTordoStartPos.getZ());
				cameraLookDirection = cameraPos;
				glm::quat rq = glm::angleAxis(rot, glm::vec3(rotAxis.getX() + (90.0f), rotAxis.getY(), rotAxis.getZ()));
				cameraPos = cameraPos + glm::rotate(rq, cameraOffset);

			}

			if (mVisibleWorld[i]->getName() == "Dice1") {
				btTransform theDice1Transform;
				theDice1->getMotionState()->getWorldTransform(theDice1Transform);
				btVector3 theDice1Pos = theDice1Transform.getOrigin();
				// Need info about rotation/orientation of cube as well
				btQuaternion theDice1Orientation = theDice1Transform.getRotation();
				// Find quaternion to take cube to new orientation
				btQuaternion transQuat = theDice1Orientation * theDice1StartOrientation.inverse();
				btVector3 rotAxis = transQuat.getAxis();
				btScalar rot = transQuat.getAngle();

				// Translate 3D cube to origin           
				// Apply new orientation
				// Translate to new position
				// This is done in reverse order of course

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(theDice1Pos.getX(), theDice1Pos.getY(), theDice1Pos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f)*theDice1StartPos.getX(), (-1.0f)*theDice1StartPos.getY(), (-1.0f)*theDice1StartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);
			}
			if (mVisibleWorld[i]->getName() == "Dice2") {
				btTransform Dice2Transform;
				Dice2Transform = theDice2->getWorldTransform();
				btVector3 Dice2Pos = Dice2Transform.getOrigin();
				btQuaternion Dice2Quat = Dice2Transform.getRotation();
				btQuaternion tq = Dice2Quat * theDice2StartOrientation.inverse();
				btVector3 rotAxis = tq.getAxis();
				btScalar rot = tq.getAngle();

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(Dice2Pos.getX(), Dice2Pos.getY(), Dice2Pos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f)*theDice2StartPos.getX(), (-1.0f)*theDice2StartPos.getY(), (-1.0f)*theDice2StartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);
			}

			// Need to set up shader attributes and uniforms before rendering object.
			// Methods of defaultShader object will throw a runtime_error exception if
			// there is an error detected in the shader processing.
			// This code should therefore be wrapped in a try..catch block

			// Associate vertex shader inputs with vertex attributes

			glEnableVertexAttribArray(defaultShader->attribute("vPosition"));
			glBindBuffer(GL_ARRAY_BUFFER, mVisibleWorld[i]->getVertexBufferID());
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(defaultShader->attribute("vNormal"));
			glBindBuffer(GL_ARRAY_BUFFER, mVisibleWorld[i]->getNormalBufferID());
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

			// Set up uniforms for materials values for this object
			glm::vec4 ambientContrib = theLight.ambientColour * mVisibleWorld[i]->getAmbientReflectivity();
			glm::vec4 diffuseContrib = theLight.diffuseColour * mVisibleWorld[i]->getDiffusiveReflectivity();
			glm::vec4 specularContrib = theLight.specularColour * mVisibleWorld[i]->getSpecularReflectivity();
			float shininess = mVisibleWorld[i]->getShininess();

			glUniform4fv(defaultShader->uniform("ambientContrib"), 1, glm::value_ptr(ambientContrib));
			glUniform4fv(defaultShader->uniform("diffuseContrib"), 1, glm::value_ptr(diffuseContrib));
			glUniform4fv(defaultShader->uniform("specularContrib"), 1, glm::value_ptr(specularContrib));
			glUniform4fv(defaultShader->uniform("lightPosition"), 1, glm::value_ptr(theLight.position));
			glUniform1f(defaultShader->uniform("shininess"), shininess);

			// Object model transform
			glm::mat4 mtb = mVisibleWorld[i]->getModelTransform();
			glm::mat4 mtm = mVisibleWorld[i]->getModelMovement();
			glm::mat4 mt = mtm * mtb;
			if (correctForBullet) mt = phys2VisTransform * mt;
			glUniformMatrix4fv(defaultShader->uniform("M"), 1, GL_FALSE, glm::value_ptr(mt));
			mVisibleWorld[i]->render();
			// Calculate view transform
			glm::vec3 cp = glm::mat3(phys2VisTransform) * cameraPos;
			glm::vec3 cld = glm::mat3(phys2VisTransform) * cameraLookDirection;
			glm::vec3 cu = glm::mat3(phys2VisTransform) * cameraUp;

			if (!useTopDownCamera)
				view = glm::lookAt(cp, cld, cu);
			else
				view = topDownCamera->getViewMatrix();


			// Associate view matrix with shader uniform V
			glUniformMatrix4fv(defaultShader->uniform("V"), 1, GL_FALSE, glm::value_ptr(view));
			// Associate projection matrix with shader uniform P
			glUniformMatrix4fv(defaultShader->uniform("P"), 1, GL_FALSE, glm::value_ptr(projection));

			mVisibleWorld[i]->render();

		}
	}

	SDL_GL_SwapWindow(window);
}

glm::vec3 Game::getVisibleWorldCentroid()
{
	size_t nObjects = mVisibleWorld.size();
	glm::vec3 c(0.0f, 0.0f, 0.0f);
	for (size_t i = 0; i < nObjects; ++i) {
		c += mVisibleWorld[i]->getMeshCentroid();
	}
	return c / (float)nObjects;
}

void Game::centerVisibleWorld()
{
	glm::vec3 cd = (-1.0f)*getVisibleWorldCentroid();
	size_t nObjects = mVisibleWorld.size();
	for (size_t i = 0; i < nObjects; ++i) {
		mVisibleWorld[i]->translateMesh(cd);
	}

}

void Game::TordoMove() {
	btTransform ct = theTordo->getWorldTransform();
	btMatrix3x3 m = ct.getBasis();
	ct.setOrigin(ct.getOrigin() - 1.0*m.getColumn(1));
	theTordo->setWorldTransform(ct);
//	thePhysicsWorld->contactPairTest(theTordo, theDice1, collisionCallback);
}
/*void Game::Load_string(char * text, SDL_Color clr, int txtNum, const char* file, int ptsize) {
	TTF_Font* tmpfont;
	tmpfont = TTF_OpenFont(file, ptsize);

	SDL_Surface *sText = SDL_DisplayFormatAlpha(TTF_RenderUTF8_Solid(tmpfont, text, clr));
	area[txtNum].x = 0; area[txtNum].y = 0; area[txtNum].w = sText->w; area[txtNum].h = sText->h;
	glGenTextures(1, &texture[txtNum]);
	glBindTexture(GL_TEXTURE_2D, texture[txtNum]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, sText->w, sText->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, sText->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	SDL_FreeSurface(sText);
	TTF_CloseFont(tmpfont);

}
void Game::drawText(float coords[3], int txtNum) {
	glBindTexture(GL_TEXTURE_2D, texture[txtNum]);
	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS); {
		glTexCoord2f(0, 0); glVertex3f(coords[0], coords[1], coords[2]);
		glTexCoord2f(1, 0); glVertex3f(coords[0] + area[txtNum].w, coords[1], coords[2]);
		glTexCoord2f(1, 1); glVertex3f(coords[0] + area[txtNum].w, coords[1] + area[txtNum].h, coords[2]);
		glTexCoord2f(0, 1); glVertex3f(coords[0], coords[1] + area[txtNum].h, coords[2]);
	} glEnd();
	glDisable(GL_TEXTURE_2D);
}*/

/*void Game::RenderText(std::string message, SDL_Color color, int x, int y, int size)
{
glMatrixMode(GL_MODELVIEW);
glPushMatrix();
glLoadIdentity();

gluOrtho2D(0, screenWidth, 0, screenHeight); // m_Width and m_Height is the resolution of window
glMatrixMode(GL_PROJECTION);
glPushMatrix();
glLoadIdentity();

glDisable(GL_DEPTH_TEST);
glEnable(GL_TEXTURE_2D);
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

GLuint texture;
glGenTextures(1, &texture);
glBindTexture(GL_TEXTURE_2D, texture);

const char* c= message.c_str();
TTF_Font * font = TTF_OpenFont("pathToFont.ttf", size);
SDL_Surface * sFont = TTF_RenderText_Blended(font,c, color);

glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFont->w, sFont->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, sFont->pixels);

glBegin(GL_QUADS);
{
glTexCoord2f(0, 0); glVertex2f(x, y);
glTexCoord2f(1, 0); glVertex2f(x + sFont->w, y);
glTexCoord2f(1, 1); glVertex2f(x + sFont->w, y + sFont->h);
glTexCoord2f(0, 1); glVertex2f(x, y + sFont->h);
}
glEnd();

glDisable(GL_BLEND);
glDisable(GL_TEXTURE_2D);
glEnable(GL_DEPTH_TEST);

glMatrixMode(GL_PROJECTION);
glPopMatrix();
glMatrixMode(GL_PROJECTION);
glPopMatrix();

glDeleteTextures(1, &texture);
TTF_CloseFont(font);
SDL_FreeSurface(sFont);
}*/


/*SDL_Texture * Game::loadTexture(std::string path)
{
// Create texture from surface pixels (using surface renderer from main window)
SDL_Texture* pTexture = SDL_CreateTextureFromSurface(pRenderer, pSurface);

if (NULL == pTexture)
{
cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << endl;
return NULL;
}

// Get rid of surface as it has now been converted to a texture, if this is not done
// then a memory leak will occur every time an image is loaded
SDL_FreeSurface(pSurface);

return pTexture;
}*/








