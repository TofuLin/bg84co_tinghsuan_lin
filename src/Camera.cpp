#include "Camera.h"

void Camera::setViewMatrix()
{
	// Now fill in the view matrix from the p,u,v,n values
	viewMatrix =
		glm::transpose(
			glm::mat4(
				glm::vec4(u.x, u.y, u.z, -1.0*glm::dot(p, u)),
				glm::vec4(v.x, v.y, v.z, -1.0*glm::dot(p, v)),
				glm::vec4(n.x, n.y, n.z, -1.0*glm::dot(p, n)),
				glm::vec4(0.0, 0.0, 0.0, 1.0)
			)
		);
}

Camera::Camera(glm::vec3 pos, glm::vec3 target, glm::vec3 up)
{
	// Calculate p, u, v, n according to the given equations
	p = pos;
	n = glm::normalize(pos - target);
	u = glm::normalize(glm::cross(up, n));
	v = glm::cross(n, u);

	setViewMatrix();
}

void Camera::move(glm::vec3 dvec)
{
	p = p + dvec;

	// Actually the only thing that changes in the view matrix is the final column
	// So this resets a number of values unnecessarily.
	setViewMatrix();
}

void Camera::roll(float angle)
{
	float cs = cos(angle);
	float sn = sin(angle);
	glm::vec3 t = u;
	u = glm::vec3(cs*t[0] - sn*v[0],
		cs*t[1] - sn*v[1],
		cs*t[2] - sn*v[2]);
	v = glm::vec3(sn*t[0] + cs*v[0],
		sn*t[1] + cs*v[1],
		sn*t[2] + cs*v[2]);
	setViewMatrix();
}