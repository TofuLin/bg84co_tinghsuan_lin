
#include "stdafx.h"

#include "CppUnitTest.h"

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/epsilon.hpp> 
#include <glm/ext.hpp>

#include <cmath>

#include <iostream>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

float const M_PI_2 = acos(0);

namespace CameraTest
{		
	TEST_CLASS(CameraTest)
	{
	public:
		 
		// Some tests to check my understanding of glm::mat4 objects
		TEST_METHOD(MatrixEquality1) {
			// Default glm::mat4 object is identity
			glm::mat4 m1; 
			glm::mat4 m2(1.0f);
			// Can't use Assert::AreEqual(m1, m2);
			// unless we provide an overloaded ToString function for glm::mat4
			Assert::IsTrue(m1 == m2);
		}

		TEST_METHOD(MatrixInequality1) {
			glm::mat4 m1;
			glm::mat4 m2(2.0f);
			Assert::IsFalse(m1 == m2);
		}
		
		// A test using epsilonEqual
		TEST_METHOD(EpsilonEqualMatrix) {
			glm::mat4 m1;
			glm::mat4 m2(1.000001f);
			// Probably need to write a utility function that would do this.
			bool b1 = glm::all(glm::epsilonEqual(m1[0], m2[0], 0.00001f));
			bool b2 = glm::all(glm::epsilonEqual(m1[1], m2[1], 0.00001f));
			bool b3 = glm::all(glm::epsilonEqual(m1[2], m2[2], 0.00001f));
			bool b4 = glm::all(glm::epsilonEqual(m1[3], m2[3], 0.00001f));
			Assert::IsTrue(b1 && b2 && b3 && b4);
		}

		// A test using epsilonEqual
		TEST_METHOD(EpsilonEqualMatrix2) {
			glm::mat4 m1;
			glm::mat4 m2(1.0001f);
			bool b1 = glm::all(glm::epsilonEqual(m1[0], m2[0], 0.00001f));
			bool b2 = glm::all(glm::epsilonEqual(m1[1], m2[1], 0.00001f));
			bool b3 = glm::all(glm::epsilonEqual(m1[2], m2[2], 0.00001f));
			bool b4 = glm::all(glm::epsilonEqual(m1[3], m2[3], 0.00001f));
			Assert::IsFalse(b1 && b2 && b3 && b4);
		}

		// Camera tests start here
		TEST_METHOD(GetViewMatrixTest1)
		{
			// A camera at the origin looking down the  -Z axis with the up direction the +Y axis.
			Camera c(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			glm::mat4 actualVM = c.getViewMatrix();
			Assert::IsTrue(expectedVM == actualVM);
		}

		// Note that with those camera parameters the view matrix is expected to be the identity!
		TEST_METHOD(TestLookAtIdentityViewMatrix) {
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			glm::mat4 actualVM; // Default constructor makes an identity matrix.
			Assert::IsTrue(expectedVM == actualVM);
		}

		TEST_METHOD(GetViewMatrixTest2)
		{
			// A camera at the origin looking down the X axis with the up direction the +Y axis.
			Camera c(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
			glm::mat4 actualVM = c.getViewMatrix();
			Assert::IsTrue(expectedVM == actualVM);
		}

		TEST_METHOD(MoveLeftTest) {
			// A camera at the origin looking down the  -Z axis with the up direction the +Y axis.
			Camera c(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			// Move the camera 1 unit to the left along the X axis.
			c.move(glm::vec3(-1, 0, 0));
			glm::mat4 actualVM = c.getViewMatrix();
			// Calculate expected view matrix
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(-1, 0, 0), glm::vec3(-1, 0, -1), glm::vec3(0, 1, 0));
			Assert::IsTrue(actualVM == expectedVM);
		}

		TEST_METHOD(MoveRightTest) {
			// A camera at the origin looking down the  -Z axis with the up direction the +Y axis.
			Camera c(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			// Move the camera 1 unit to the right along the X axis.
			c.move(glm::vec3(1, 0, 0));
			glm::mat4 actualVM = c.getViewMatrix();
			// Calculate expected view matrix
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(1, 0, 0), glm::vec3(1, 0, -1), glm::vec3(0, 1, 0));
			Assert::IsTrue(actualVM == expectedVM);
		}

		TEST_METHOD(MoveDiagonalTest) {
			// A camera at the origin looking down the  -Z axis with the up direction the +Y axis.
			Camera c(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			// Move diagonally (1,1) units in the X-Z plane
			c.move(glm::vec3(1, 0, 1));
			glm::mat4 actualVM = c.getViewMatrix();
			// Calculate expected view matrix
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(1, 0, 1), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
			Assert::IsTrue(actualVM == expectedVM);
		}

		TEST_METHOD(RollLeftTest) {
			// A camera at the origin looking down the  -Z axis with the up direction the +Y axis.
			Camera c(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			// Rotate 90 degrees = pi/2 radians counterclockwise
			c.roll(M_PI_2);
			glm::mat4 actualVM = c.getViewMatrix();
			// Calculate expected view matrix
			glm::mat4 expectedVM = glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
			expectedVM = glm::rotate(expectedVM, float(M_PI_2), glm::vec3(0, 1, 0));
			Assert::IsTrue(actualVM == expectedVM);
		}
	};
}