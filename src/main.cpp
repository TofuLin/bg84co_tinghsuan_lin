
#include "Game.h"

int main(int argc, char *argv[])
{
	Game * theGame = new Game();

	theGame->initialise();
	theGame->run();
	theGame->shutdown();

	return 0;
}