#pragma once

// Use GLEW for access to OpenGL 
#define GLEW_STATIC
#include <GL/glew.h>

// SDL for windowing and input capture
#include <SDL.h>
#include <SDL_opengl.h>
#include"SDL_ttf.h"
// GLM for mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <string>
#include <map>

#include <functional>

#include "btBulletDynamicsCommon.h"

#include "VisibleObject.h"
#include "Camera.h"


//#if (_Win32)
#include <windows.h>
//#endif

struct Light {
	glm::vec4 position;
	glm::vec4 diffuseColour;
	glm::vec4 ambientColour;
	glm::vec4 specularColour;
};

struct PlayerData {
	std::string name;
	int money;
	int health;
};

enum GameState {
	Start = 0,
	Pause,
	Quit
};

class KeyHandler {
public:
	KeyHandler(std::function<void(float)> f) : action(f) {}
	void operator() (float d) { action(d); }
	void run(float d) { action(d); }
private:
	std::function<void(float)> action;
};



struct MyContactResultCallback : public btDiscreteDynamicsWorld::ContactResultCallback {
	btScalar virtual addSingleResult(btManifoldPoint& cp,
		const btCollisionObjectWrapper* colObj0Wrap,
		int partId0,
		int index0,
		const btCollisionObjectWrapper* colObj1Wrap,
		int partId1,
		int index1,
		const btCollisionObjectWrapper* colObj2Wrap,
		int partId2,
		int index2)
	{
		const btRigidBody* rb0 = btRigidBody::upcast(colObj0Wrap->getCollisionObject());
		const btRigidBody* rb1 = btRigidBody::upcast(colObj1Wrap->getCollisionObject());
		const btRigidBody* rb2 = btRigidBody::upcast(colObj2Wrap->getCollisionObject());
		std::string rb0Name = (*bodyNames).at(const_cast<btRigidBody*>(rb0));
		std::string rb1Name = (*bodyNames).at(const_cast<btRigidBody*>(rb1));

		std::string rb2Name = (*bodyNames).at(const_cast<btRigidBody*>(rb2));
		return 0;
	}
	int collisionCount = 0;
	std::map<btRigidBody*, std::string>* bodyNames;
};
class Card{
	int type;
	int value;
};
class Classroom{
public:
	char className[15];
	int id;//deferent id present dierent type classroom
	int type;
	int value;//money should pay to bank
	int rent[3];
	//int update();
	//update classroom
};

class Game
{
public:
	GameState State;
	void initialise();
	void run();
	void shutdown();


protected:

	virtual void readConfigFile();
	virtual void bindKeyboard();

	virtual void loadAssets();

	virtual void update(SDL_Keycode aKey, float delta);
	virtual void render();

	std::vector<VisibleObject*> mVisibleWorld;

	SDL_Window* window;
	SDL_GLContext context;

	int screenWidth, screenHeight;
	int movestep;
	// Model, view and projection matrix uniform locations
	GLuint ModelTransformLoc, ViewTransformLoc, ProjectionTransformLoc;

	glm::mat4 view;
	glm::mat4 projection;

	Light theLight;

	//GLuint shaderProgram;
	ShaderProgram * defaultShader;

	glm::vec3 getVisibleWorldCentroid();
	void centerVisibleWorld();
	//void RenderText(std::string message, SDL_Color color, int x, int y, int size);
	/*void drawText(float coords[3], int txtNum);
	GLuint texture[100];
	SDL_Rect area[100];
	SDL_Surface *SDL_DisplayFormatAlpha(SDL_Surface *surface);
	void Load_string(char * text, SDL_Color clr, int txtNum, const char* file, int ptsize);*/
private:
	std::string configFile;
	std::vector<std::string> assetFiles;

	std::map<std::string, std::string> keyBindings;
	std::map<std::string, KeyHandler*> commandHandler;

	float angle = 0.1f; // amount to rotate view.
	glm::mat4 viewRotMatrix;

	// Visible world stuff;
	Camera* topDownCamera;
	bool useTopDownCamera = false;

	glm::vec3 cameraPos;
	glm::vec3 cameraOffset;
	glm::vec3 cameraLookDirection;
	glm::vec3 cameraUp;

	// Physics stuff
	std::string  physicsFilename;
	bool isPhysicsPaused = true;
	glm::mat4 phys2VisTransform;
	bool correctForBullet = true;
	bool music = true;
	btDiscreteDynamicsWorld* thePhysicsWorld;
	btRigidBody* theTordo;
	btVector3 theTordoStartPos;
	btQuaternion theTordoStartOrientation;
	btRigidBody* theDice1;
	btVector3 theDice1StartPos;
	btQuaternion theDice1StartOrientation;
	btRigidBody* theDice2;
	btVector3 theDice2StartPos;
	btQuaternion theDice2StartOrientation;

	//MyContactResultCallback collisionCallback;

	std::map<btRigidBody*, std::string> rigidBodyNames;

	float theTordoRotationAmount = 0.5f;
	/*void RenderString(float x, float y, const char* string)
	{
	char *c;

	glColor3f(1.0, 0.2, 0.2);
	glRasterPos2f(x, y);

	glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, *c);
	}
	*/
	/*SDL_Texture* loadTexture(std::string path);
	TTF_Font* loadFont(std::string path, int pointSize);
	SDL_Texture* createTextTexture(std::string text, TTF_Font* pFont, SDL_Color colour, int style);
	TTF_Font* pFont20Pt = NULL;
	SDL_Texture* pPlayerNameTex = NULL;
	const SDL_Color GOLD{ 0xFF, 0xD7, 0x00, 0xFF };
	SDL_Renderer* pRenderer = NULL;
	SDL_Surface* pSurfact;*/
	void TordoMove();
};